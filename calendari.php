<?php
	session_start();
	include('header.php');
	include('php/funcions.php');
	$matricula = validacio($_SESSION['matricula']);
	$seldies= "SELECT dia FROM data";
?>
<!--En aqui es mostrarà la taula amb el calendari, tot amb javascript, en la que es pot moure entre els diferents mesos i 
els dies, en els quals es podran selecionar i per tant, el post l'agafara d'aqui.-->
<!--Si el dia no estigues disponible, es marcaria en gris clar, i no deixaria que es marqués.-->
	<article class="container cos-pagina">
		<section class="row">
			<h2>Selecciona el dia del mes</h2>
		</section>
		<h4>			<?php echo "Matricula: ".$matricula.""?>		</h4>
		<section class="column seccio-central">
			<section class="column" id="titols">
				<h2 id="nommes"></h2><h2 id="anyu"></h2>
			</section>
			<table id="diescal" class="calendari">
				<tr id="fila0"><th></th><th></th><th></th><th></th><th></th><th></th><th></th></tr>
				<tr id="fila1"><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>
				<tr id="fila2"><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>
				<tr id="fila3"><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>
				<tr id="fila4"><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>
				<tr id="fila5"><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>
				<tr id="fila6"><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>
			</table>
				<section class="botons-calendar row">
			<a class="flecha column-20" id="anterior" onclick="mesant()"><img src="img/left.png"></a>
    	<a class="flecha column-20" id="seguent" onclick="messeg()"><img src="img/right.png"></a>
		</section>
			<div class="row submit">
				<form method="POST" action="diahora.php">
					<input type="hidden" name="dia" id="dia">
					<input type="hidden" name="mes" id="mes">
          <input type="hidden" name="any" id="any">
					<input type="button" onclick="history.go(-1);" value="Enrere">
					<input type="submit" id="calsubmit" class="button" value="Següent">
				</form>
			</div>
			<?php
				if($resultat = mysqli_query($conn,$seldies)){
					echo "<ul class='nodisplay'>";	
					if(mysqli_num_rows($resultat)>0){
						 while($row = mysqli_fetch_array($resultat)) {
							 echo "<li class='dies_complets' >".$row['dia']."</li>";
						}
					}
					echo "</ul>";
				}
				else{
					echo mysqli_error($conn);
				}			
			?>
		</section>
	</article>
	<?php include('footer.php');?>