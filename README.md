# README #
VERSIO 1.8
This README would normally document whatever steps are necessary to get your application up and running.

# Integrants del projecte #
Els integrants d'aquest Projecte son:
	Kevin Sequera Ríos,
	Victor Poderoso De Moya,
	Cristina García Rodríguez
	
# Objectiu “breu” del projecte #
L'objectiu d'aquest projecte es generar una web interactiva, en la que es pugui demanar/eliminar una cita per a passar la ITV, 
Aquesta web ha de permetre a un Admin veure les hores que estan disponibles en una base de dades, i al client ensenyar-li les hores disponibles.
En un principi només es un projecte per a Cotxes, pero que es pot evolucionar a més vehicles.

# Estat “breu” del projecte #
Aconseguida la interaccio amb la base de dades i creacio dels selects, inserts, deletes i updates.
Afegit el calendari en JavaScript
Creacio del llistat d'hores seleccionables
 
# Actualitzant directoris i afegint la part del motor de connexio a la BBDD #
Creat el core PHP en la versio mes basica

# Adreça web de producció de cada integrant del grup # 
https://docs.google.com/a/iam.cat/spreadsheets/d/1XczNmQkpXDZ8nJTC4DWEqXwu3uxnsOUD0XyCzvvZntU/edit?usp=sharing

# Adreça moqups #

https://app.moqups.com/a16vicpodmoy@iam.cat/IbqoaHOdet/view/page/abf6a4278

# Adreça web de la documentació phpdoc (labs.iam.cat) #
IN PROCESS
# Adreça web del projecte desplegat (labs.iam.cat) #

labs.iam.cat/~a16kevseqrio/iam-motors-projecte-transversal/
