<?php 
	session_start();
	include('header.php');
	include('php/funcions.php');
	$_SESSION['nom'] = validacio($_POST['nom']);
	$_SESSION['cognoms'] = validacio($_POST['cognoms']);
	$_SESSION['email'] = validacio($_POST['email']);
	$_SESSION['telefon'] = validacio($_POST['telefon']);
	
	
?>
<article class="container cos-pagina">
	<section class="row">
		<h2>Confirmacio de dades</h2>
	</section>
	<section class="seccio-central">
		<p class="row">
			Aquesta es la informacio que s'enregistrara a la nostra base de dades, si us plau, comproveu que son correctes abans de fer click a enviar:
		</p>
		<br>
		<div class="row">
			<table>
				<tr>
					<th>Matricula</th>
					<th>Nom</th>
					<th>Cognoms</th>
					<th>Dia</th>
					<th>Mes</th>
					<th>Hora</th>
				</tr>
				<tr>
					<td>
						<?php echo $_SESSION['matricula'] ?>
					</td>
					<td>
						<?php echo $_SESSION['nom'] ?>
					</td>
					<td>
						<?php echo $_SESSION['cognoms'] ?>
					</td>
					<td>
						<?php echo $_SESSION['dia'] ?>
					</td>
					<td>
						<?php echo $_SESSION['mes'] ?>
					</td>
					<td>
						<?php echo $_SESSION['hora']; ?>
					</td>
				</tr>
			</table>
		</div>
		<hr>
		<section class="row submit">
			<a class="button" href="diahora.php">Retrocedir</a>
			<a class="button" href="insertar.php">Enviar</a>
		</section>

	</section>
</article>
<?php include('footer.php');?>