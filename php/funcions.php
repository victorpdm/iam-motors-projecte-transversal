<?php

  function validacio($dades){
    $dades = trim($dades);
    $dades = stripslashes($dades);
    $dades = htmlspecialchars($dades);
    return $dades;
    
  }


  function validacio_mail($email){
    
    if(!filter_var($email, FILTER_VALIDATE_MAIL)){
      $emailerr="E-mail invalid";
    }
    return $emailerr;
  }
  // case switch maybe?

  function convertirdia($data){
    if($data == 'January'){
      $data = 'Gener';
      return $data;
    }
    if($data == 'February'){
      $data = 'Febrer';
      return $data;
    }
    if($data == 'March'){
      $data = 'Març';
      return $data;
    }
    if($data == 'April'){
      $data = 'Abril';
      return $data;
    }
    if($data == 'May'){
      $data = 'Maig';
      return $data;
    }
    if($data == 'June'){
      $data = 'Juny';
      return $data;
    }
    if($data == 'July'){
      $data = 'Juliol';
      return $data;
    }
    if($data == 'August'){
      $data = 'Agost';
      return $data;
    }
    if($data == 'September'){
      $data = 'Septembre';
      return $data;
    }
    if($data == 'October'){
      $data = 'Octubre';
      return $data;
    }
    if($data == 'November'){
      $data = 'Novembre';
      return $data;
    }
    if($data == 'December'){
      $data = 'Desembre';
      return $data;
    }
    
    
    
  }

?>