$(document).ready(function() {
	
	//Formulari index
	$("#matricula").focus();
	$("input").prop('required', true);
	$('#matricula').on('input change',function() {
		//todos los inputs con nombre acabado en zip, la i es por ser Key Sensitive 
		if ((!$(this).val().match(/^[A-Z]{0,2}\d{4}([B-D]|[F-H]|[J-N]|[P-T]|[V-Z]){3}$/)) ){
			//border color canviat a vermell i disabled button
			$(this).css('border-color', '#FF7171');
			$('input[name="submit"]').prop('disabled', true);
		}
		else {
			//Al estar tot correcte et deixa fer la entrada de la matricula
			$(this).css('border', '');
			$('input[name="submit"]').prop('disabled', false);
			$('.warning').css('display', 'none');
		}
		
	});
	

	//Comprovacion de datos : Telefono
	
	$('#telefon').on('input change',function(){
		if(!$(this).val().match(/^[6|7|9][0-9]{8}$/)){
			$(this).css('border-color', '#FF7171');
			$('input[name="submit"]').prop('disabled', true);
		}
		else{
			$(this).css('border-color', '#B2FFAD');
			$('input[name="submit"]').prop('disabled', false);
		}
	})
	//Comprovacion datos : E-mail
	$('#email').keyup(function(){
		if(!$(this).val().match( /^[a-z0-9\._-]+@[a-z0-9-]{2,}[.][a-z]{2,4}$/)){
			$(this).css('border-color', '#FF7171');
			$('input[name="submit"]').prop('disabled', true);
		}
		else{
			$(this).css('border-color', '#B2FFAD');
			$('input[name="submit"]').prop('disabled', false);
		}
	})
	
		/* Dies disponibles */
	//Crearem un array amb els dies que ens generara mitjançant una consulta SQL
	var dies_ocupats = [];
	
	//Recorrerem la llista de dies oculta al calendari per poder enmagatzemar els dies dins l'array dies_ocupats.
	$('#dies_ocupats').first().find('li').each(function(index,value){
		dies_ocupats.push($(this).html());
	});
	console.log(dies_ocupats);
	$('tr').find('td').each(function(){
		for(var i =0 ; i<dies_ocupats.length; i++){
			console.log()
			if($(this).text()==dies_ocupats[i]){
				console.log($(this).html()+" = "+dies_ocupats[i]);
				
			}
		}
	});

	/*Seccio calendari:
		Quan es seleciona un td, o sigui, un dia, aquest es remarcara amb un border.
		Si ja hi ha un selecionat, treura la selecio d'aquest i el posara al que s'ha selecionat nou.
		Els diumenge no es poden selecionar: donen uan alert avisant d'aixo
	*/


	$('td').click(function(){
		
		if($(this).hasClass('diumenge')){
			alert("Els diumenge no hi ha cites");
		} else if ($(this).hasClass('noDisp')) {
			alert("Dia no disponible");
		} else if ($(this).hasClass('avui')) {
			alert("Dia no disponible");
		} else {
			$("td").removeClass("selected");
			$(this).addClass("selected");
			var valordia = $(this).html();
			var valormes = $('#nommes').html();
			var valorany = $('#anyu').html();
			$('#dia').val(valordia);
			$('#mes').val(valormes);
			$('#any').val(valorany);
		}
	});
	

	

	
	
	
	//Aqui compararem el valor dels dies 
	
	/*Seccio hores: 
	Quan apretem una de les hores, aquesta es queda marcada, i ens fa un display ocult d'aquesta. 
	Se li afegeix una Class a aquesta selecciohnada, cosa que es va removent d'una i altres si ja hi ha alguna selecionada.
	Falta fer la comprobació de les hores que estan agafades i fer que el display no es pugui selecionar.
	*/
	$('.blochora').click(function() {
		$('.blochora').removeClass("horaSel");
		$(this).addClass("horaSel");
		var valorhora = $(this).html();
		$('#inputhora').val(valorhora);
	}); 

	/* Hores disponibles */
	//Crearem un array on guardarem els valors d'una llista que ens generara una consulta en SQL
	var hores_ocupades = [];

	//Aqui guardem els valors de una llista oculta que teiem mitjançant una query a la pagina diahora
	$('#hores_ocupades').first().find('li').each(function(index,value) {
			hores_ocupades.push($(this).text());
		console.log($(this).text());
	});
	
	//Aqui comparem els valors del array de llistas, amb els valors de la lista de blocks de hores.
	$('#selecciohores li').each(function(){
		for(var i =0 ; i<hores_ocupades.length; i++){
			if($(this).text()==hores_ocupades[i]){
				console.log($(this).text()+" = "+hores_ocupades[i]);
				$(this).addClass('noDisp');
			}
		}
	});
	
		

});