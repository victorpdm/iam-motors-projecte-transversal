//Arrays
messos = ["Gener ", "Febrer ", "Març ", "Abril ", "Maig ", "Juny ", "Juliol ", "Agost ", "Septembre ", "Octubre ", "Novembre ", "Desembre "];
nom_dia_setmana = ["Diumenge", "Dilluns", "Dimarts", "Dimecres", "Dijous", "Divendres", "Dissabte"];
dias_setmana = ["Dll", "Dt", "Dm", "Dj", "Dv", "Ds", "Dg"];


//S'iniciara al arrancar la pagina
window.onload = function() {

  //Data actual
  avui = new Date();
  dia_sem_act = avui.getDay(); //dia setmana actual
  dia_act = avui.getDate(); //dia mes actual
  mes_act = avui.getMonth(); //mes actual
  any_act = avui.getFullYear(); //any actual
  // Afegim elements en el dom
  titmes = document.getElementById("nommes");
  titanyu = document.getElementById("anyu");
  ant = document.getElementById("anterior");
  pos = document.getElementById("seguent");
  f0 = document.getElementById("fila0");

  mescal = mes_act; //mes principal
  anycal = any_act; //any principal
  //iniciem funcions del calendari:
  capcalera();
  prim_linia();
  imp_dies();
  llista();

}

//FUNCIONES---------------------------------------------------
//capçalera
function capcalera() {
  titmes.innerHTML = messos[mescal];
  titanyu.innerHTML = anycal;
  mes_ant = mescal - 1; //mes anterior
  mes_seguent = mescal + 1; //mes seguent
  if (mes_ant < 0) {
    mes_ant = 11;
  }
  if (mes_seguent > 11) {
    mes_seguent = 0;
  }
}

//Imprimir nom dels dies de la setmana ----------------------------
function prim_linia() {
  for (i = 0; i < 7; i++) {
    fila0 = f0.getElementsByTagName("th")[i];
    fila0.innerHTML = dias_setmana[i];
    fila0.style.textAlign = "center";
    fila0.style.padding = "20px 20px";
  }
}


//Omplim les caselles amb els dies ---------------------------------

function imp_dies() {
  //Nom del dia de la setmana del dia 1 del mes
  prim_dia_mes = new Date(anycal, mescal, "1") //busquem el dia del primer mes
  nom_dia_setmana = prim_dia_mes.getDay(); //Obtenim el nom del dia de la setmana del dia 1
  nom_dia_setmana--; //Restem un dia per adptarlo a que començi la setmana per dilluns, per defecte ho fa el diumenge
  if (nom_dia_setmana == -1) nom_dia_setmana = 6;

  //busquem el dia per la primera cassella
  dia_primer_mes = prim_dia_mes.getDate();
  primer_dia_cassella = dia_primer_mes - nom_dia_setmana; //reste els dies que falten de la setmana
  comencar = prim_dia_mes.setDate(primer_dia_cassella); //Quan comença
  diames = new Date(); //creem l'objecte
  diames.setTime(comencar); //I li assignem la data de la primera cassella

  //Examinem totes les caselles
  for (i = 1; i < 7; i++) { //localitzem la fila

    fila = document.getElementById("fila" + i);

    for (j = 0; j < 7; j++) {

      el_meu_dia = diames.getDate()
      el_meu_mes = diames.getMonth()
      el_meu_any = diames.getFullYear()
      cassella = fila.getElementsByTagName("td")[j];
      cassella.innerHTML = el_meu_dia;

      //Recuperem l'estil inicial al cambiar de mes

      cassella.style.color = "#492736";
      cassella.style.backgroundcolor = "#FFFFFF";

      //padding && margin de totes les caselles:

      cassella.style.padding = "20px 20px";
      cassella.style.marginBottom = "3px";

      //align de text en totes les caseller td

      cassella.style.textAlign = "center";

      //Dies diferents del mes actual. Text
      
      if (el_meu_any > any_act) {
        cassella.className = "disp";

      }
      if (el_meu_any < any_act) {
        cassella.className = "noDisp";
      }
      if (el_meu_mes!=mescal) {
        cassella.className = "noDisp";
      }
      if(el_meu_mes<mes_act && el_meu_any==any_act){
        cassella.className = "noDisp";
      }

      //destacar el dia actual, se li afegeix una clase unica anomenada Avui
      if (el_meu_mes == mes_act && el_meu_dia == dia_act && el_meu_any == any_act) {
        cassella.innerHTML = el_meu_dia;
        cassella.className = "avui";
      }

      //dies passats o sigui NO DISPONIBLES
      if (el_meu_dia < dia_act && el_meu_mes == mes_act && el_meu_any == any_act) {
        cassella.className = "noDisp";
      }
      /*if (el_meu_dia > dia_act && el_meu_mes < mes_act && el_meu_dia < dia_act) {
        cassella.className = "noDisp";
      }*/
      if(el_meu_mes < mes_act && el_meu_any< any_act){
         cassella.className = "noDisp";
      }
      //pinta tots el dies anteriors, o sigui els passats
      
      if ((el_meu_dia > dia_act && el_meu_mes == mes_act && el_meu_any == any_act) || (el_meu_dia < dia_act && el_meu_mes > mes_act && el_meu_any == any_act)) {
        cassella.className = "disp";
        
      }
 

      //posem els diumenges en vermell els hi passem una class per a que no es puguin seleccionar en cap moment

      if (j == 6) {
        cassella.style.backgroundColor = "#C14C57";
        cassella.className = "diumenge";
      }

      //cassella.className = "noDisp";
      //Amb la sentencia anterior posarem la clase noDisp a els dies 
      //anteriors al que tenim marcat com a actual, això fara que no es pugui marcar.

      //pasem al seguent
      el_meu_dia = el_meu_dia + 1;
      diames.setDate(el_meu_dia);
    }
  }
}

//Veure mes anterior -----------------------------

function mesant() {
  nou_mes = new Date()
  prim_dia_mes--;
  nou_mes.setTime(prim_dia_mes) //modifiquem la data al mes anterior
  mescal = nou_mes.getMonth() //adaptem les variables al nou valor
  anycal = nou_mes.getFullYear()
  capcalera()
  imp_dies()
}

//veure mes seguent -------------------------------
function messeg() {
  nou_mes = new Date()
  /*En les dos següents variables primer establim dies_anyadit amb la data actual
  que estigui seleccionada al calendari i en el seguent cas li anyadim 45 dies perque
  començi el mes següent*/
  dies_anyadit = prim_dia_mes.getTime()
  dies_anyadit += (45 * 24 * 60 * 60 * 1000)
  nou_mes.setTime(dies_anyadit) //fecha con mes posterior.
  mescal = nou_mes.getMonth() //adaptem les variables al nou valor
  anycal = nou_mes.getFullYear()
  capcalera()
  imp_dies()
}
