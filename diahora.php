<?php
	session_start();
	include('header.php');
	include('php/funcions.php');
	$_SESSION['dia'] = validacio($_POST['dia']);
  $_SESSION['mes'] = validacio($_POST['mes']);
	$dia = $_SESSION['dia'];
	$mes = $_SESSION['mes'];

	$selhores = "SELECT hora FROM diames NATURAL JOIN hores WHERE dia=$dia AND mes='$mes'";
?>
	<article class="container cos-pagina">
		<h3>Selecciona una de les hores disponibles</h3>
		<h4>
			<?php echo "Matricula: ".$_SESSION['matricula']." Dia: ".$_SESSION['dia']." Mes: ".$_SESSION['mes'];?>
		</h4>
		<section class="column seccio-central">
			<section class="row seccio-hores">
				<form method="POST" action="dadescita.php">
						<ul id="selecciohores">
								<?php
									$start = '08:00:00';
									$end = '20:00:00';
									$time = strtotime($start);
									$timeStop = strtotime($end);
									while($time<$timeStop) {
										for($i=0; $i<6; $i++){
											echo "<li class='blochora' >".date('H:i', $time)."</li>";
											$time = strtotime('+30 minutes', $time);
										}
										echo "<br>";
									}
							?>
						</ul>
					<div class="row submit" id="buttonsHora">
						<input type="hidden" id="inputhora" name="hora">
						<input type="button" onclick="history.go(-1);" value="Enrere">
						<input type="submit" name="submit" value="Següent" class="horasubmit">
					</div>
				</form>
				
			</section>
		</section>
	</article>
	<section>
				<?php	
					if($resultat = mysqli_query($conn,$selhores)){
					echo "<ul id='hores_ocupades' class='nodisplay'>";	
					if(mysqli_num_rows($resultat)>0){
						 while($row = mysqli_fetch_array($resultat)) {
							 echo "<li>".$row['hora']."</li>";
						}
					}
					echo "</ul>";
				}
				else{
					echo mysqli_error($conn);
				}
				?>
				</section>
	<?php include('footer.php');?>