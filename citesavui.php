<?php 
	session_start();
	include('header.php');
  include('php/funcions.php');
	$constdia = "j";
	$constmes = "F";
  $dia = Date($constdia);
	$mes = Date($constmes);
	$mes = convertirdia($mes);
	
?>
<article class="container cos-pagina">
	<section class="row">
		<h2>Confirmacio de dades</h2>
	</section>
	<section class="seccio-central">
		<div class="row">
			<?php 
					$query = "SELECT * FROM matricula NATURAL JOIN dadesclient NATURAL JOIN diames NATURAL JOIN hores WHERE dia=$dia AND mes='$mes'";	
			
					if($resultat = mysqli_query($conn,$query)){
					if(mysqli_num_rows($resultat)>0){
			?>
			<table class="table" align-content="center">
				<tr>
					<th>Hora</th>
					<th>Nom</th>
					<th>Cognoms</th>
					<th>Matricula</th>
					<th>Telèfon</th>
				</tr>
				<?php
							while($row = mysqli_fetch_array($resultat)){
								echo "<tr><td>".$row['hora']."</td><td>".$row['nom']."</td><td>".$row['cognoms']."</td><td>".$row['matricula']."<td>".$row['telefon']."</td></tr>";
							}
						}
						else{
							echo "No s'han trobat cites per avui";
						}
						
					}
					else{
						echo mysqli_error($conn);
					}	
			?>
			</table>
		</div>
		<hr>
		<section class="enviadades" style="justify-content: space-around">
			<a id="ap" class="button" href="area_personal.php">Enrere</a><a id="ap" class="button" href="index.php">Sortir</a></li>
		</section>

	</section>
</article>
<?php include('footer.php');?>