<?php 
	session_start();
	include('header.php');

?>
<article class="container">
	<h2>Area del Personal</h2>
	<section class="seccio-central">
		<section class="column">
			<h5>Espai per al personal del centre.</h5>
			<p></p>
		</section>
		<section class="column">
				<section class="row">
					<button class="column" id="anterior" onclick="mesant()">Anterior</button>
					<button class="column" id="seguent" onclick="messeg()">Següent</button>
				</section>
				<section class="row">
					<h2 id="titols"></h2>
				</section>
				<table id="diescal" class="calendari">
					<tr id="fila0"><th></th><th></th><th></th><th></th><th></th><th></th><th></th></tr>
					<tr id="fila1"><th></th><th></th><th></th><th></th><th></th><th></th><th></th></tr>
					<tr id="fila2"><th></th><th></th><th></th><th></th><th></th><th></th><th></th></tr>
					<tr id="fila3"><th></th><th></th><th></th><th></th><th></th><th></th><th></th></tr>
					<tr id="fila4"><th></th><th></th><th></th><th></th><th></th><th></th><th></th></tr>
					<tr id="fila5"><th></th><th></th><th></th><th></th><th></th><th></th><th></th></tr>
					<tr id="fila6"><th></th><th></th><th></th><th></th><th></th><th></th><th></th></tr>
				</table>
				<div class="row submit">
					<!-- S'ha d'afegir un buscador a aquesta seccio, que fagi una busqueda del dia, mes i any a la BBDD. -->
						<button type="submit" value="Següent">
				</div>
			
		</section>
</article>

<?php include('footer.php')?>