<?php
  require('php/login.php');
?>
<!DOCTYPE html>
<html lang="en">
<!--Tots els fitxers a tenir en compte, amb els estils: milligram i normalize, i el propi.
També estan els javascripts/jquery-->
<head>
	<meta charset="UTF-8">
	<title>IAM motors</title>
	<link rel="stylesheet" href="css/normalize.css">
	<link rel="stylesheet" href="css/milligram.min.css">
	<link rel="stylesheet" href="css/estils.css">
	<script type="text/javascript" src="js/jquery-3.2.1.js"></script>
	<script type="text/javascript" src="js/calendari.js"></script>
	<script type="text/javascript" src="js/funcions.js"></script>

</head>
<!-- En el body está la imatge de fons, igual que la taula de navegació, que contindra només un enllaç a 
la pagina d'inici i a l'area personal-->
<body>
	<header>
		<div class="container cabecera">
			<h1>IAM motors</h1>
			<div class="imatge"><img src="img/logo6.png"></div>
		</div>
	</header>
	<div class="menu container-fluid">
		<div class="container menucontenidor">
			<a href="index.php" class="column-20">Inici</a>
			<a href="area_personal.php" class="column-20"><img id="access" src="img/loginImg3.png"></a>
		</div>
	</div>