DROP DATABASE IF EXISTS cites;
CREATE DATABASE cites;
USE cites;

CREATE TABLE dadesclient (
	email varchar (100) PRIMARY KEY,
	nom varchar (30),
	cognoms varchar (50),
	telefon varchar (12)
);

CREATE TABLE matricula (
	email varchar (100),
	matricula varchar (7) PRIMARY KEY,
	FOREIGN KEY (email) REFERENCES dadesclient(email) ON DELETE CASCADE ON UPDATE CASCADE
);



CREATE TABLE diames (
	matricula varchar (7),
	dia int UNSIGNED,
	mes varchar (10),
	FOREIGN KEY (matricula) REFERENCES matricula(matricula) ON DELETE CASCADE ON UPDATE CASCADE

);

CREATE TABLE hores (
	matricula varchar (7),
	hora varchar (5),
	FOREIGN KEY (matricula) REFERENCES matricula(matricula) ON DELETE CASCADE ON UPDATE CASCADE
	
);

CREATE TABLE data(
	dia int PRIMARY KEY,
	mes varchar (10)
	
);

INSERT INTO dadesclient VALUES ('victor@gmail.com','Victor','Poderoso','629367695');
INSERT INTO matricula VALUES ('victor@gmail.com','4567SSS');
INSERT INTO diames VALUES ('4567SSS',29,'Desembre');
INSERT INTO hores VALUES ('4567SSS','08:00');

INSERT INTO dadesclient VALUES ('josep@gmail.com','Josep','Martenc','629367695');
INSERT INTO matricula VALUES ('josep@gmail.com','5555LLK');
INSERT INTO diames VALUES ('5555LLK',29,'Desembre');
INSERT INTO hores VALUES ('5555LLK','08:30');

INSERT INTO dadesclient VALUES ('Marta@gmail.com','Marta','Torralba','629367695');
INSERT INTO matricula VALUES ('victor@gmail.com','6666LLH');
INSERT INTO diames VALUES ('6666LLH',29,'Desembre');
INSERT INTO hores VALUES ('6666LLH','09:00');

INSERT INTO dadesclient VALUES ('alex@gmail.com','Alexandre','Guillaud','629367695');
INSERT INTO matricula VALUES ('victor@gmail.com','7777LLX');
INSERT INTO diames VALUES ('7777LLX',29,'Desembre');
INSERT INTO hores VALUES ('7777LLX','09:30');



/*
delimiter //
CREATE TRIGGER `comptahores` AFTER UPDATE ON `diames`
 FOR EACH ROW BEGIN
	DECLARE comptador INT;
	SELECT COUNT(hora) INTO comptador FROM hores NATURAL JOIN diames WHERE dia=NEW.dia AND mes=NEW.mes;
	IF a > 4 THEN 
		INSERT INTO data VALUES(NEW.dia,NEW.mes);
	END IF;
END;//
delimiter ;

delimiter //
CREATE TRIGGER `comptahores` AFTER DELETE ON `diames`
 FOR EACH ROW BEGIN
	DECLARE comptador INT;
	SELECT COUNT(hora) INTO comptador FROM hores NATURAL JOIN diames WHERE dia=NEW.dia AND mes=NEW.mes;
	IF a > 4 THEN 
		INSERT INTO data VALUES(NEW.dia,NEW.mes);
	END IF;
END;//
delimiter ;

delimiter //
CREATE TRIGGER `comptahores` AFTER INSERT ON `diames`
 FOR EACH ROW BEGIN
	DECLARE comptador INT;
	SELECT COUNT(hora) INTO comptador FROM hores NATURAL JOIN diames WHERE dia=NEW.dia AND mes=NEW.mes;
	IF a > 4 THEN 
		INSERT INTO data VALUES(NEW.dia,NEW.mes);
	END IF;
END;//
delimiter ;
*/