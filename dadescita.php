<?php
	session_start();
	include('header.php');
	include('php/funcions.php');
	$_SESSION['hora']=validacio($_POST['hora']);
?>
	<article class="container cos-pagina">
		<section class="row">
			<h2>Introdueix les teves dades personals</h2>
		</section>
		<h3>
			<?php echo "Matricula: ".$_SESSION['matricula']." Dia: ".$_SESSION['dia']." Mes: ".$_SESSION['mes']." Hora: ".$_SESSION['hora'];?>
		</h3>
		<section class="row seccio-central">
			<form method="POST" action="ficita.php">
				<input id="nom" type="text" name="nom" placeholder="Nom" autofocus>
				<input id= "cognom" type="text" name="cognoms" placeholder="Cognom">
				<input id="email" type="email" name="email" placeholder="E-mail">
				<input id="telefon" type="text" name="telefon" placeholder="Télefon">
				<div class="row submit">
					<input type="button" onclick="history.go(-1);" value="Enrere">
					<input type="submit" name="submit" value="Enviar">
				</div>
			</form>
			
		</section>
	</article>

	<?php include('footer.php');?>