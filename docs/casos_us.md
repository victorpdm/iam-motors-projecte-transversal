# Casos ús #

En aquí podrem veure els diferents casos d'ús que hem anat veient o pensant.

# 1 #

Funcionament correcte de la página:
L'usuari entra la seva matrícula, Si la matricula ja te una cita reservada, la aplicacio ens dura a una pagina on es mostrara les
seves dades, conjuntament amb el dia la hora i el mes que la va solicitar.

# 2 #

En cas de que ja existis, te la opcio de modificar-la o si es aixi, ens retornara al calendari i començara tot el proces de nou fins inserir les dades,
si l'usuari vol esborrar la matricula, nomes haura de premer el boto de Esborrar.


# 3 #

En cas de que l'usuari no comptes amb una cita al sistema, haura d'inserir la matricula, escollir el dia i la hora
quan vol entrar les seves dades personals, no entra correctament un email, donant un error, s'ha deixat el @ o el .com/.cat/.net/.es
Se li remarca la casella dient que ha de posar un correu valid.
Un cop fet, se li retorna la pantalla de confirmació.

# 4 #

L'usuari entra una matrícula: aquesta no es correcte. 
Ha de tenir : 
  1 lletra devant 4 numeros i 2 lletres darrere.
  2 lletres devant 4 numeros i 2 lletres darrere.
  4 numero 3 lletres darrere sense ser vocals.
Quan l'escriu correctament l'envia a selecionar l'hora.

# 5 #

Tres usuaris entren, els tres entren la matrícula i soliciten la mateixa hora del mateix dia.
Dos d'ells entren les dades personals amb 1 minut, el tercer amb 2.
Als dos primers sel's hi confirma la selecció.
Aquest últim se li avisa que no hi ha plaça per a l'hora demanada. Se li ha de retornar a la selecció de dia/mes.

# 6 #

Quan un usuari seleciona la ultima hora disponible d'un dia, al següent li ha d'apareixer en gris, o sigui
Sense poderlo seleccionar

# 7 #

L'usuari entra una matrícula, el dia i la hora.
Si ja ha fet la ITV en menys d'un any, se li diu que no pot reservar-la fins el mateix dia que la va fer o posterior.
Se'l retorna a Inici.

# 8 #

Usuari modifica entrada pero no la guarda, es te que mantenir la reserva anterior.

# 9 #

Usuari modifica o esborra la reserva, es té que esborrar de la BBDD. Si posa alguna cosa malament, ha de donar error.

# 10 #

Usuari vol esborrar una reserva. Introdueix la matrícula, si ja te una reserva, se li pregunta si vol modificar o esborrar.
Clicka esborrar: seguidament se li pregunta el correu. Si ho introdueix bé se li mostren les dades.
Si està d'acord, se li esborra la reserva, si ha cambiat d'opinio, pot tirar enrere i tornar a l'inici.

# 11 1#


